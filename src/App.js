/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  ImageBackground,
  StyleSheet
} from 'react-native';
import Player from "./player";
import { videos } from "./videos";

export default class App extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      playVideoIndex: -1,
      isPlaying: false
    }
  }

  /**
   * play video on click
   */
  playVideo = (selectedIndex) => {
    /**
     * When user click on thumbnail stop all other video if it is playing and start clicked video
     */
    if (this.state.playVideoIndex != selectedIndex) {
      this.setState({ playVideoIndex: selectedIndex });
    }
  }

  /**
   * Render player 
   * you need to create thumbnail for YouTube video because you can not create a parallel instance in android 
   * while you can create parallel instance in ios if you want
  */
  renderYouTube = (rowData) => {
    return (
      <View style={{ marginTop: 10, marginBottom: 10 }}>
        {
          /**
           * if current playing index is not equal than show thumbnail otherwise show video
           */
          this.state.playVideoIndex != rowData.index ?
            <TouchableOpacity
              style={Styles.thumbnail} onPress={() => this.playVideo(rowData.index)}
            >
              <ImageBackground
                source={{ uri: `http://i3.ytimg.com/vi/${rowData.item.id}/hqdefault.jpg` }}
                style={Styles.thumbnail}
              >
                <Image source={require("./youtube-logo.png")} />

              </ImageBackground>
            </TouchableOpacity>
            :
            <Player
              item={rowData.item}
            />
        }

      </View>
    )
  }

  render() {
    return (
      <SafeAreaView>
        <View style={{ padding: 20 }}>
          <FlatList
            data={videos}
            renderItem={this.renderYouTube}
            keyExtractor={(item, index) => index.toString()}
            extraData={this.state}
          />
        </View>
      </SafeAreaView>
    )
  }
}


const Styles = StyleSheet.create({
  thumbnail: {
    alignSelf: 'stretch',
    height: 300,
    alignItems: 'center',
    justifyContent: 'center'
  }
})
