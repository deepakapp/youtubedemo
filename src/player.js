import React from "react";
import YouTube from 'react-native-youtube';

/**
 * 
 * @param {*} props 
 * Custom player component
 */
function Player(props){
    return(
    <YouTube
        apiKey={"AIzaSyCYTI_fj1QFOXIcgWoiCgqoPQQeqcUKto0"}
        videoId={props.item.id} // The YouTube video ID
        play // control playback of video with true/false
        loop // control whether the video should loop when ended
        onReady={e => { }}
        onChangeState={e => { }}
        onChangeQuality={e => { }}
        onError={e => { }}
        style={{ alignSelf: 'stretch', height: 300 }}
        //ref={item => this.player = item}
    />
    )
};
export default Player;